﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class smoothLoc : MonoBehaviour
{
    private List<InputDevice> inputDevices = new List<InputDevice>();
    private InputDevice manG;
    private InputDevice manD;
    private bool clickedG;
    private bool clickedD;

    public GameObject maneG;
    public GameObject maneD;

    public float speed;
    
    // Start is called before the first frame update
    void Start()
    {
        InputDevices.GetDevices(inputDevices);

        foreach (InputDevice device in inputDevices){
            //si c'est une manette
            if(device.characteristics.HasFlag(InputDeviceCharacteristics.Controller) && device.characteristics.HasFlag(InputDeviceCharacteristics.HeldInHand)){
                if(device.characteristics.HasFlag(InputDeviceCharacteristics.Left)){
                    this.manG = device;
                }else if (device.characteristics.HasFlag(InputDeviceCharacteristics.Right)){
                    this.manD = device;
                }
            }
        }

        clickedG = false;
        clickedD = false;
    }

    // Update is called once per frame
    void Update()
    {
        manG.TryGetFeatureValue(CommonUsages.secondaryButton, out bool secG);
        manG.TryGetFeatureValue(CommonUsages.deviceVelocity, out Vector3 velG);
        manG.TryGetFeatureValue(CommonUsages.primaryButton, out bool primG);

        manD.TryGetFeatureValue(CommonUsages.secondaryButton, out bool secD);
        manD.TryGetFeatureValue(CommonUsages.deviceVelocity, out Vector3 velD);
        manD.TryGetFeatureValue(CommonUsages.primaryButton, out bool primD);

        Rigidbody toApplyforce = GetComponent<Rigidbody>();

        if(secG){
            Debug.Log("Bouton gauche");
            Vector3 michel = new Vector3(maneG.transform.forward.x, 0f, maneG.transform.forward.z);
            michel *= (speed + (Mathf.Abs(velG.x + velG.y + velG.z)*600));
            toApplyforce.AddForce(michel);

        }
        if(secD){
            Debug.Log("Bouton droit");
            Vector3 michel = new Vector3(maneD.transform.forward.x, 0f, maneD.transform.forward.z);
            michel *= (speed + (Mathf.Abs(velD.x + velD.y + velD.z)*600));
            toApplyforce.AddForce(michel);
        }

        if(primG){
            if(!clickedG){
                Debug.Log("rotation gauche");
                transform.Rotate(new Vector3(0f, -45f, 0f));
                clickedG = true;
            }
        }else{
            clickedG = false;
        }
        if(primD){
            if(!clickedD){
                Debug.Log("rotation droite");
                transform.Rotate(new Vector3(0f, 45f, 0f));
                clickedD = true;
            }
        }else{
            clickedD = false;
        }
    }
}
