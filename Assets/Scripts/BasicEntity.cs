﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BasicEntity : MonoBehaviour
{
    public GameObject lui;
    public int hp;
    public Text text;

    // Start is called before the first frame update
    void Start()
    {
        DamagePopupController.initialize();
    }

    // Update is called once per frame
    void Update()
    {
        if(hp <= 0){
            Debug.Log("I DED");
            Destroy(lui);
        }
    }

    public void takeDamage(float dmg){
        Debug.Log("OOF, i took " + dmg.ToString() + " damage.");
        DamagePopupController.createFloatingText(dmg.ToString(), new Vector3(transform.position.x, transform.position.y + 1, transform.position.z));
        hp -= Mathf.RoundToInt(dmg);
        if(hp <= 0){
            text.text = "DED";
        }else{
            text.text = hp.ToString();
        }
    }
}
