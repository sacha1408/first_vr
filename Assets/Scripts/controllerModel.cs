﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class controllerModel : MonoBehaviour
{
    public List<GameObject> controllerPrefabs;
    public InputDeviceCharacteristics caracs;
    private InputDevice targetDevice;
    private GameObject spawnedController;
    
    // Start is called before the first frame update
    void Start()
    {
        List<InputDevice> devices = new List<InputDevice>();
        InputDevices.GetDevicesWithCharacteristics(caracs, devices);

        targetDevice = devices[0];
        GameObject prefab = controllerPrefabs.Find(controller => controller.name == targetDevice.name);

        if(prefab){
            spawnedController = Instantiate(prefab, transform);
        }else{
            Debug.LogError("manette pas trouvée");
            spawnedController = Instantiate(controllerPrefabs[0], transform);
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
