﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fire : MonoBehaviour
{

    public gunScript gun;

    private bool loaded;

    private bool clicked;

    public Material act;
    public Material desact;

    // Start is called before the first frame update
    void Start()
    {
        loaded = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(gun.ball != null){
            loaded = true;
        }
    }

    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.name == "Left Hand" || other.gameObject.name == "Right Hand" && !clicked){
            GetComponent<MeshRenderer>().material = act;
            if(loaded){
                gun.fire();
                loaded = false;
            }
            Debug.Log("click");
            clicked = true;
        }
    }

    private void OnTriggerExit(Collider other) {
        if(other.gameObject.name == "Left Hand" || other.gameObject.name == "Right Hand" && clicked){
            GetComponent<MeshRenderer>().material = desact;
            Debug.Log("out click");
            clicked = false;
        }
    }
}
