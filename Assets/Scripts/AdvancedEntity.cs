﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AdvancedEntity : MonoBehaviour
{
    public GameObject lui;
    private Part[] parts; 
    public int hp;
    public Text text;

    // Start is called before the first frame update
    void Start()
    {
        DamagePopupController.initialize();
        parts = GetComponentsInChildren<Part>();
    }

    // Update is called once per frame
    void Update()
    {
        if(hp <= 0){
            Debug.Log("I DED");
            Destroy(lui);
        }
    }

    public void takeDamage(float dmg){
        hp -= Mathf.RoundToInt(dmg);
        DamagePopupController.createFloatingText(dmg.ToString(), new Vector3(transform.position.x, transform.position.y + 1, transform.position.z));
        if(hp <= 0){
            text.text = "DED";
        }else{
            text.text = hp.ToString();
        }
    }
}
