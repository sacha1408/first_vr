﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gunScript : MonoBehaviour
{

    public GameObject gun;
    private FixedJoint joint;
    private SphereCollider trig;

    public GameObject ballHolder;
    public AudioSource gunSound;
    public GameObject ball;

    // Start is called before the first frame update
    void Start()
    {
        trig = GetComponent<SphereCollider>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other) {
        if(other.gameObject.name == "Ball"){
            other.transform.position = ballHolder.transform.position;
            joint = gun.gameObject.AddComponent<FixedJoint>();
            joint.connectedBody = other.GetComponent<Rigidbody>();
            joint.breakForce = 1000f;
            ball = other.gameObject;
        }
    }

    public void fire(){
        if (ball != null){
            //fire
            gunSound.Play();
            joint.connectedBody = null;
            ball.GetComponent<Rigidbody>().AddForce(transform.forward * 1000f);
            ball = null;
            Debug.Log("Boom");
        }
    }
}
