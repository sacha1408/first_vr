﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagePopupController : MonoBehaviour
{
    private static DmgPopup popText;
    public static void initialize(){
        if(!popText){
            popText = Resources.Load<DmgPopup>("Prefabs/popParent");
        }
    }
    public static void createFloatingText(string text, Vector3 location){
        DmgPopup instance = Instantiate(popText);
        instance.setText(text);
        instance.transform.position = location;
    }
}
