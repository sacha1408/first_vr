﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DmgPopup : MonoBehaviour
{
    public Animator animator;
    private Text dmgText;

    // Start is called before the first frame update
    void Start()
    {
        AnimatorClipInfo[] clipInfo = animator.GetCurrentAnimatorClipInfo(0);
        Destroy(gameObject, clipInfo[0].clip.length);
        dmgText = animator.GetComponent<Text>();
    }

    public void setText(string text){
        dmgText.text = text;
    }
}
