﻿using UnityEngine;
using UnityEngine.UI;

public class PointsChamboule : MonoBehaviour
{
    private bool alreadyHit;
    public Text score;

    void Start(){
        alreadyHit = false;
    }


    void OnCollisionEnter(Collision col){
        if(!alreadyHit){
            if(col.gameObject.name == "Ball"){
                Debug.Log("OOF");
                alreadyHit = true;
                //ajouter des points
                int scoreInt = int.Parse(this.score.text) + 100;
                this.score.text = scoreInt.ToString();
            }
        }
    }
}
