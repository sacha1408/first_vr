﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class Sword : MonoBehaviour
{

    public float damage;
    private Rigidbody rb;
    private BoxCollider coll;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    float calcDamage(){
        Vector3 vel = rb.velocity;
        float currVelocity = Mathf.Abs(vel.x + vel.y + vel.z);
        return (Mathf.Round(damage*currVelocity *100f)/100f);
    }

    private void OnTriggerEnter(Collider other) {
        if(other.name == "Dummy"){
            other.GetComponent<BasicEntity>().takeDamage(calcDamage());
        }
    }
}
