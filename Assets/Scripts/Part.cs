﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Part : MonoBehaviour
{

    private int hp;

    private bool damaged = false;

    private GameObject obj;
    // Start is called before the first frame update
    void Start()
    {
        obj = GetComponent<GameObject>();
    }

    // Update is called once per frame
    void Update()
    {
        if(damaged){
            Destroy(obj);
        }
    }

    public void takeDamage(float dmg){
        Debug.Log("OOF, i took " + dmg.ToString() + " damage on the " + obj.name);
        hp -= Mathf.RoundToInt(dmg);
        GetComponentInParent<AdvancedEntity>().takeDamage(dmg);
        if(hp <= 0){
            damaged = true;
        }
    }
}
